# To-Do List

## Demo using Loom

* Narrated Demo (https://www.loom.com/share/06f9e2a9f16b46c4b8f063ad6bb97c7b)

##Stakeholder Requirements

* Android Studio
* login page (each with: email account and password)
* task page (each with: title, description, due date)
* save tasks to Firebase-Realtime or Firestone
* page to display all tasks for logged in user
* at least 5 commits
* BitBucket
* modularization 

##Thought Process on Stakeholder Requirements:

* Android Studio
	* ( in Java )
* login page
	* ( login AND signup page for new users, requires input validation ) 
* task page (each with: title, description, due date)
	* ( 3 fields per task -> each task a JSON object )
* save tasks to Firebase-Realtime or Firestone
	* ( noSQL, each user has a node JSON object with numbering for each task which is a nested JSON object)
* page to display all tasks for logged in user
	* ( read and append textField per task into the main page field )
* at least 5 commits
* BitBucket

* NOTE: added requirement on 4/19 that 'AllTasks' page must be implemented using a RecyclerView, originally was going to implement with ListView
	
##Activities + it's .xml in layout:

* Sign- In page
* Sign- Up page
* All- Tasks page
* Create Task page

##Steps:

* User Sign In and Sign Up Authentication with Firebase Authentication (X)
* Read from Firebase Realtime to populate All-Tasks page (with button + Intent to go to Create Task page ) 
* Create Task page - Create into Firebase Realtime

## Extra Features nice to have:

* using Google API on EACH task to add into Google Calendar
* Delete and Edit Task

## Limitations

* email must follow particular format ( must not contain '/', '.', '#', '$', '[', or ']' because used it as key) -> can fix if needed for email validation later on
* further email/data validation of both email and password
* Sign Out button
* using a DATE object rather than String for the date field of each Task
* tested using Pixel 2 API 27 running Android 9.0 Pie
* dependency versioning
