package com.example.todolist;

import java.util.HashMap;

public class User {
    /* user class that will be used to create the initial object into Realtime Database */
    /* HashMap of HashMap's, with each inner JSON object being a task */

    public HashMap<String, HashMap <String, Object> >tasks;
    //inner hashMap should have order, title, description, date (so 4 inner fields )

    public User(){

    }

    public User(String id){
    }
    /* String is just going to be a number of the task that it is */
    /* inner HashMap are the 3 fields: title, description, date */

    /* whenever put a new task within the inner HashMap so new task, then need to increment count and use that as the String key */
    public User(String id,String inputTitle, String inputDescription, String inputDate){
        //according to Firebase doc, default required
        /* create the intial task with title 0 */
        tasks = new HashMap<String, HashMap <String,Object>> ();

        HashMap <String,Object> individualTask= new HashMap <> ();

        individualTask.put("title", inputTitle);
        individualTask.put("description", inputDescription);
        individualTask.put("date", inputDate);

        tasks.put(id,individualTask);
    }
    public HashMap<String, HashMap <String, Object> > getTasks(){
        return tasks;
    }

}
