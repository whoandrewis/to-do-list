package com.example.todolist;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

public class TaskViewHolder extends RecyclerView.ViewHolder{
    TextView post_title;
    TextView post_desc;
    TextView post_date;
    View mView;

    public TaskViewHolder(View itemView){
        super(itemView);
        mView=itemView;
        post_title= (TextView)mView.findViewById (R.id.tasktitle);
        post_desc= (TextView)mView.findViewById (R.id.taskdescription);
        post_date= (TextView)mView.findViewById (R.id.taskdate);
    }
    public void setTitle (String title){
        post_title.setText(title);
    }
    public void setDesc (String desc){
        post_desc.setText(desc);
    }
    public void setDate (String date){
        post_date.setText(date);
    }
}
