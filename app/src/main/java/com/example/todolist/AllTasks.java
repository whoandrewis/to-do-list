package com.example.todolist;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.ArrayList;
import java.util.List;


public class AllTasks extends AppCompatActivity {

    FloatingActionButton add;
    private FirebaseAuth mAuth;
    /* to get the current UserID which was directed from either MainActivity or Register */

    private String email;
    private String UID;

    private RecyclerView display;
    private DatabaseReference mDatabase;
    private FirebaseRecyclerAdapter<Task, TaskViewHolder> adapter;

    private Query query;
    private ArrayList<Task> arraylist;
    private FirebaseRecyclerOptions<Task> options;

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_tasks);
        FirebaseApp.initializeApp(this);

       /* currUserID=FirebaseAuth.getInstance().getCurrentUser();
        it's the previous person who was signed in
        instead pass through using Extras

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        Toast.makeText(this, user.getEmail() + "All Tasks", Toast.LENGTH_SHORT).show();
        */

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            email = extras.getString("key");
            UID = extras.getString("uid");
            //The key argument here must match that used in the other activity
            // Toast.makeText(this, email + "All Tasks", Toast.LENGTH_LONG).show();
        }

        add = findViewById(R.id.fab);
        add.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                toAddTask();
            }
        });

        mDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(UID).child(email).child("tasks");
        mDatabase.keepSynced(true);
        arraylist = new ArrayList<Task>();

        display = (RecyclerView) findViewById(R.id.myrecycleview);
        display.setHasFixedSize(true);
        display.setLayoutManager(new LinearLayoutManager(this));

        super.onStart();
        query = FirebaseDatabase.getInstance().getReference().child("Users").child(UID).child(email).child("tasks");
        options =
                new FirebaseRecyclerOptions.Builder<Task>()
                        .setQuery(query, Task.class)
                        .build();

        adapter = new FirebaseRecyclerAdapter<Task, TaskViewHolder>(options) {
            @Override
            public TaskViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                // Create a new instance of the ViewHolder, in this case we are using a custom
                // layout called R.layout.message for each item
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.task_row, parent, false);
                return new TaskViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(@NonNull TaskViewHolder holder, int position, Task model) {

                holder.post_title.setText(model.getTitle());
                holder.post_date.setText(model.getDate());
                holder.post_desc.setText(model.getDescription());

            }

        };

        display.setAdapter(adapter);

    }

    private void toAddTask() {
        Intent intent= new Intent(this, AddTask.class);
        intent.putExtra("key",email);
        intent.putExtra("uid", UID);
        this.startActivity(intent);
    }

}
