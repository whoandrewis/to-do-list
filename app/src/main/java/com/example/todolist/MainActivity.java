package com.example.todolist;

import android.app.Dialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.Toolbar;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseError;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private Button buttonRegist;

    Button buttonSignIn;

    FirebaseAuth mAuth;

    AutoCompleteTextView mEmailView;
    EditText mPasswordView;
    String email;
    String password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /* DO: method to INITIALIZE FIELDS */

        FirebaseApp.initializeApp(this);
        mAuth = FirebaseAuth.getInstance();

        mEmailView = findViewById(R.id.email);
        mPasswordView = findViewById(R.id.password);

        /* user login -> if success then intent to the next task page */
        /* if fail then setContent to this page and message that error */
        buttonSignIn = findViewById(R.id.email_sign_in_button);
        buttonSignIn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                email = mEmailView.getText().toString().trim();
                password = mPasswordView.getText().toString().trim();
                openSignIn();
            }

        });

        buttonRegist = findViewById(R.id.email_register);
        buttonRegist.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                openRegister();
            }
        });
    }

    public void openSignIn(){

        if (TextUtils.isEmpty(email)) {
            Toast.makeText(getApplicationContext(), "Please enter email...", Toast.LENGTH_LONG).show();
            return;
        }
        if (TextUtils.isEmpty(password)) {
            Toast.makeText(getApplicationContext(), "Please enter password!", Toast.LENGTH_LONG).show();
            return;
        }

        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this,new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(getApplicationContext(), "Login successful!", Toast.LENGTH_LONG).show();
                            String modifiedEmail= email.substring(0, email.length()-4);

                            Intent intent = new Intent(MainActivity.this, AllTasks.class);
                            intent.putExtra("key",modifiedEmail);

                            String currentuser = FirebaseAuth.getInstance().getCurrentUser().getUid();
                            intent.putExtra("uid", currentuser);

                            startActivity(intent);
                        }
                        else {
                            Toast.makeText(getApplicationContext(), "Login failed! Please try again later", Toast.LENGTH_LONG).show();

                        }
                    }
                });
    }






    public void openRegister() {
        /* press the register button so intent to go to the register class */

        Intent intent = new Intent(this, Register.class);
        this.startActivity(intent);
    }


}

