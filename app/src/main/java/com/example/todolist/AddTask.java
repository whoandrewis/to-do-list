package com.example.todolist;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;

public class AddTask extends AppCompatActivity {

    private Button submitTaskButton;
    private String email;
    private EditText viewTitle;
    private EditText viewDescription;
    private EditText viewDate;

    private String title;
    private String description;
    private String date;

    private DatabaseReference mDatabase;
    private String UID;

    private double currCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_task);
        FirebaseApp.initializeApp(this);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            email = extras.getString("key");
            UID=extras.getString("uid");

            //The key argument here must match that used in the other activity
            Toast.makeText(this, email + "All Tasks", Toast.LENGTH_LONG).show();
        }

        viewTitle=findViewById(R.id.titleTask);
        viewDescription=findViewById(R.id.descriptionTask);
        viewDate=findViewById(R.id.dateOfTask);

        submitTaskButton=findViewById(R.id.submitTask);
        submitTaskButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                /* need to initialize the entries to each of the fields */

                /* DO: validate each of the fields */

                title = viewTitle.getText().toString();
                description = viewDescription.getText().toString();
                date=viewDate.getText().toString();

                addTask();
            }

        });
    }

    private void addTask() {
        /* use SET for the count, because want to increment that field */

        mDatabase = FirebaseDatabase.getInstance().getReference("Users");
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef= database.getReference("Users");

        String idNewTask = myRef.push().getKey();
        Map <String,Object> tasks = new HashMap<>();

        HashMap <String,Object> individualTask= new HashMap <> ();

        individualTask.put("title", title);
        individualTask.put("description", description);
        individualTask.put("date", date);

        tasks.put(idNewTask, individualTask);
        myRef.child(UID).child(email).child("tasks").updateChildren(tasks);

        /* route back to AllTasks */
        Intent intent= new Intent(this, AllTasks.class);
        intent.putExtra("key",email);
        intent.putExtra("uid",UID);
        this.startActivity(intent);
    }

}
