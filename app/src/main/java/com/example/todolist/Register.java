package com.example.todolist;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.nfc.Tag;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
/*
Toast.makeText(this,email + " " + password, Toast.LENGTH_SHORT).show();
*/
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
/* import com.google.firebase.auth.FirebaseUser; */

public class Register extends AppCompatActivity {
    private Button buttonSignIn;

    private FirebaseAuth mAuth;

    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private String email;
    private String password;
    private DatabaseReference mDatabase;
    private String uid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        FirebaseApp.initializeApp(this);

        mAuth=FirebaseAuth.getInstance();
        mEmailView = findViewById(R.id.email);
        mPasswordView= findViewById(R.id.password);

        /* user login -> if success then intent to the next task page */
        /* if fail then setContent to this page and message that error */

        buttonSignIn= findViewById(R.id.email_register);
        buttonSignIn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                email = mEmailView.getText().toString();
                password = mPasswordView.getText().toString();
                openSignIn();
            }

        });

    }

    public void openSignIn(){

        /* DO:
        - need to verify that when registering, the username isn't already taken
        - the password is not blank
        - the email is not blank
        - the email contains an @ sign
        for now assume that valid inputs
         */

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            FirebaseUser user = mAuth.getInstance().getCurrentUser();
                            //Toast.makeText(Register.this, user.getEmail(), Toast.LENGTH_SHORT).show();
                        } else {
                            // If sign in fails, display a message to the user
                            Toast.makeText(Register.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }

                    }});


        /* Toast toast = Toast.makeText(this, "Hello "+ email, Toast.LENGTH_SHORT);
        View view = toast.getView();
        //Gets the actual oval background of the Toast then sets the colour filter
        view.getBackground().setColorFilter(Color.rgb(66, 226, 244), PorterDuff.Mode.SRC_IN);
        toast.show();
        */

        /* just created new user so need to put them into the realtime database */
        /*
        issues when using database
        mDatabase = FirebaseDatabase.getInstance().getReference();
        User user= new User (email);
        mDatabase.child("Users").child(mAuth.getInstance().getUid()).setValue(user);
        */
        mDatabase = FirebaseDatabase.getInstance().getReference("Users");
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef= database.getReference("Users");
        /* want to create a child with the username in that collection */
        /* the name of the entry in the collection will be a User object with it's id as the email */

        HashMap<String, User> userAdd = new HashMap<>();

        String modifiedEmail= email.substring(0, email.length()-4); // DO: this is assuming it ends in com

        String taskID= myRef.push().getKey();

        userAdd.put(modifiedEmail, new User(taskID));
        /* keys must not contain '/', '.', '#', '$', '[', or ']'  */
        /* DO: transform email to valid form so that it can be used as the key
        * take each non-valid character and transform */

        String UID = myRef.push().getKey();
        // create a child with index value
        // create a child node with the unique id
        myRef.child(UID).setValue(userAdd);


        /* DO: get the uid of a field */
        //Toast.makeText(this, UID, Toast.LENGTH_LONG).show();

        Intent intent = new Intent(this, AllTasks.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        // add the UID into the extra of the intent
        intent.putExtra("key",modifiedEmail);
        intent.putExtra("uid", UID);
        this.startActivity(intent);
        finish();

    }



}

